import unittest
from app.mod_feed.website.WebSiteInteractions import WebSiteInteractions
from app.mod_feed.website.models import WebSite

class TestWebsiteDB(unittest.TestCase):
    def test_simple_retrieval(self):
        self.assertIsInstance(WebSiteInteractions.check_source("arstechnica.com"), WebSite)
