import unittest
import json
import os
from app.mod_summary.summarization.preprocessing import term_frequency

TEST_DATA_FILENAME = os.path.join(os.path.dirname(__file__), 'test_data/test_fr.json')

class TestTermFrequency(unittest.TestCase):
    def setUp(self):
        self.test_data = json.load(open(TEST_DATA_FILENAME))

    def test_none_tf(self):
        with self.assertRaises(TypeError):
            term_frequency(None)

    def test_wrong_data_type(self):
        with self.assertRaises(TypeError):
            term_frequency("abc")

    def test_expected_output(self):
        for test in self.test_data["test_fr"]:
            self.assertEqual(term_frequency(test["given_data"]), test["expected_result"])

    def test_empty_list(self):
        with self.assertRaises(AttributeError):
            term_frequency([])