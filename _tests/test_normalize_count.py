import unittest
import json
import os
from app.mod_articles.preprocessing import normalize_word_count

TEST_DATA_FILENAME = os.path.join(os.path.dirname(__file__), 'test_data/test_nc.json')

class TestNormalizeWordCount(unittest.TestCase):
    def setUp(self):
        self.test_data = json.load(open(TEST_DATA_FILENAME))

    def test_normal_values(self):
        for test in self.test_data["test_nwc"]["test_function"]:
            self.assertEqual(normalize_word_count(test["data"]), test["expected"])

    def test_none_Value(self):
        with self.assertRaises(TypeError):
            normalize_word_count(None)
