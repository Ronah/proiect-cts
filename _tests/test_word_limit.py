import unittest
import json
import os
from app.mod_articles.preprocessing import limit_article

TEST_DATA_FILENAME = os.path.join(os.path.dirname(__file__), 'test_data/test_wl.json')

class TestLimit(unittest.TestCase):
    def setUp(self):
        self.test_data = json.load(open(TEST_DATA_FILENAME))

    def test_simple_limit(self):
        for test in self.test_data["test_wl"]:
            self.assertEqual(limit_article(test["limit"], test["text"]), test["expected"])

    def test_none_limit(self):
        with self.assertRaises(AttributeError):
            limit_article(None, "abc abac adv")
