import unittest

from app._tests.test_url import TestDomainRetrieval

class Test(unittest.TestCase):
    def suite(self):
        suite = unittest.TestSuite()
        suite.addTest(TestDomainRetrieval("test_null_value"))
        suite.addTest(TestDomainRetrieval("test_number_value"))
        return suite

