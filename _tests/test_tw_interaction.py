import unittest
import json
import os
from app.mod_feed.twitter.TwitterInteractions import TwitterInteraction

TEST_DATA_FILENAME = os.path.join(os.path.dirname(__file__), 'test_data/test_tw.json')

class TestTwitterInteraction(unittest.TestCase):
    def setUp(self):
        self.test_data = json.load(open(TEST_DATA_FILENAME))

    def test_user_retrieval(self):
        for test in self.test_data["test_tw"]["user_extraction"]["url"]:
            self.assertEqual(TwitterInteraction.verify_source(test["given"]), test["expected"])

    def test_null_value(self):
        with self.assertRaises(AttributeError):
            TwitterInteraction.verify_source(None)

    def test_other_type(self):
        with self.assertRaises(AttributeError):
            TwitterInteraction.verify_source(23)