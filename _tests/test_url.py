import unittest
import json
import os

from app.mod_feed.website.WebSiteInteractions import WebSiteInteractions
from app.mod_feed.website.exceptions.NoneTypeException import NoneTypeException

TEST_DATA_FILENAME = os.path.join(os.path.dirname(__file__), 'test_data/test_url.json')

class TestDomainRetrieval(unittest.TestCase):
    def setUp(self):
        self.test_data = json.load(open(TEST_DATA_FILENAME))

    def test_string_values(self):
        for test in self.test_data["test_url"]:
            if not test["fail"]:
                self.assertEqual(WebSiteInteractions.get_domain(test["data"]), test["correct"])
            else:
                with self.assertRaises(ValueError):
                    WebSiteInteractions.get_domain(test["data"])

    def test_null_value(self):
        with self.assertRaises(NoneTypeException):
            WebSiteInteractions.get_domain(None)

    def test_number_values(self):
        with self.assertRaises(AttributeError):
            WebSiteInteractions.get_domain(20)

    def test_zero(self):
        with self.assertRaises(NoneTypeException):
            WebSiteInteractions.get_domain(0)

    def test_real_values(self):
        with self.assertRaises(AttributeError):
            WebSiteInteractions.get_domain(20.56154)

    def test_very_long_values(self):
        long_url = "http://myapplication.example/mycontroller/1/myaction?hostname=www.mycustomer.example&request[pa" \
                   "ram_a]=3&request[param_b]=1&request[param_c]=0&request[param_d]=0&request[param_e]=3&request[para" \
                   "m_f]=1&request[param_g]=4&request[param_h]=0&request[param_i]=5&requestfafafwafawfwafawfafaawfawfw" \
                   "afwafwafffaa/dwad/dwa/dwa/daw/wad/wa/daw/d.com"
        self.assertEqual(WebSiteInteractions.get_domain(long_url), "myapplication.example")


