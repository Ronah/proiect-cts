import unittest
import json
import os
from app.mod_summary.summarization.preprocessing import lemmatize_words_array

class TestLemmatizer(unittest.TestCase):
    def test_none_value(self):
        with self.assertRaises(TypeError):
            lemmatize_words_array(None)

    def test_other_type(self):
        with self.assertRaises(TypeError):
            lemmatize_words_array(23)

    def test_empty_array(self):
        with self.assertRaises(TypeError):
            lemmatize_words_array([])

    def test_char_parameter(self):
        with self.assertRaises(TypeError):
            lemmatize_words_array("abc")